# Companygestionner

🙌 __Python FastAPI Companygestionner API__ 🙌

Made by Léo DEVAUX for school project in Python with FastAPI!

### Run with Docker

```bash
docker-compose up --build
```
That will start env with 3 services (Back, Mysql, PhpMyAdmin)


## Swagger Documentation

<http://localhost:8080/docs>  (docker installation default port)

## PhpMyAdmin

<http://localhost:8081>  (docker installation default port)
USER: db_user
PSW: db_user_pass

## Authentication

Don't forget to authenticate yourself to use the API, only the /login route is public.

Please note that when you will authenticate yourself, you will have to enter your email in the `username` field and your password in the `password` field.

All passwords is "azerty" non hashed

## Database information

### By default

The database is MySQL.

Please refer to the following table to understand the meaning of the characters in the database tables.

Table: activities
 
| id | name    | goal          | description                        | localisation | owner_id | planning_id |
|----|---------|---------------|------------------------------------|--------------|----------|-------------|
| 1  | Bowling | Team building | La communication au sein de l'équipe | reims        | 1        | 2           |
| 2  | Piscine | Chill         | Petite sortie à la piscine          | Strasbourg   | 1        | 1           |

Table: compagnies
| id | siret      | name   | city    | country | website      |
|----|------------|--------|---------|---------|--------------|
| 1  | 2202020    | esf    | sef     | sef     | sefsef.fr    |
| 2  | 234523132  | google | reims   | FRANCE  | www.google.fr |
| 3  | 12344      | Amazon | lille   | France  | www.amazon.Fr|

Table: planning
| id | name              | compagnie_id |
|----|-------------------|--------------|
| 1  | Le meilleur planning | 1            |
| 2  | qzdqzd           | 1            |

Table: users
| id | firstname | lastname | username  | password                                                                                      | ROLE      | id_compagny |
|----|-----------|----------|-----------|-----------------------------------------------------------------------------------------------|-----------|-------------|
| 1  | leo       | devaux   | leod1     | f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9 | MAINTENER | 1           |
| 2  | pascal    | detret   | pascalou  | f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9 | USER      | 2           |
| 3  | steven    | isidor   | sisidor   | f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9 | ADMIN     | 3           |
| 4  | thomas    | claeys   | tclaeys   | f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9 | USER      | 1           |
| 5  | estelle   | casu     | ecasu     | f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9 | MAINTENER | 2           |
| 6  | jules     | sorensen | jsorensen | f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9 | ADMIN     | 2           |


## Evaluation grid

### 1. Structure du projet et organisation du code (35 points)

-  README.md clair et bien documenté (sachant qu'un README ne me permettant pas d'exécuter le code entrainera une réduction de la note globale de 33%) (5 points)
-  Organisation des fichiers et dossiers (5 points)
-  Utilisation appropriée des modules et packages (5 points)
-  Lisibilité et propreté du code (10 points)
-  Commentaires lisibles et faisant sens (5 points)
-  Bonne utilisation du git (commits de bonne taille, messages faisant sens) (5 points)

### 2. Implémentation des standards appris en cours (35 points)

-  Utilisation de pydantic (5 points)
-  Section d'import bien formaté (system, libs, local), et chemins absolus et non relatifs Requirements.py avec versions fixes (5 points)
-  Définition du type de donnée en retour des fonctions. (5 points)
-  Bonne utilisation des path & query parameters (10 points)
-  Retour des bons codes HTTP (10 points)

### 3. Implémentation des fonctionnalités demandées (85 points)

-  Connexion à la base de données (30 points)
-  Gestion des utilisateurs (15 points)
-  Gestion des plannings (15 points)
-  Gestion des activités (15 points)
-  Gestion des entreprises (10 points)

### 4. Sécurité (20 points)

-  Utilisation de tokens pour l'authentification (JWT) (5 points)
-  Validation et vérification des données entrantes avec modèles pydantics, not (5 points)
-  Gestion des erreurs et exceptions (5 points)
-  Sécurisation de la connexion à la base de données (5 points)
