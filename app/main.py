#System imports

#Libs imports
from fastapi import FastAPI

#Local imports
from internal import auth, compagny, planning, activities, user
app = FastAPI()

app.include_router(auth.router, tags=["Authentification"])
app.include_router(compagny.router, tags=["Entreprises"])
app.include_router(planning.router, tags=["Plannings"])
app.include_router(activities.router, tags=["Activites"])
app.include_router(user.router, tags=["Utilisateurs"])
