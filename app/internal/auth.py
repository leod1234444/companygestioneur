#System imports
from typing import Annotated
import hashlib

#Libs imports
from fastapi import Depends, APIRouter, status, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from pydantic import BaseModel

#Local imports
from dbinterface.SQLDatabase import SQLDatabase


router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

class User(BaseModel):
    id_compagny: int
    id: str
    username: str
    role: str

JWT_KEY = "kajshkdalasjjlhgkjguifoudhsfkxahdsf"

async def decode_token(token: Annotated[str, Depends(oauth2_scheme)]) -> User:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        decoded_data = jwt.decode(token, JWT_KEY, algorithms=['HS256'])
        return User(
            username=decoded_data.get("username"),
            id=decoded_data["id"],
            id_compagny=decoded_data["id_compagny"],
            role=decoded_data["role"],
        )
    except JWTError:
        return credentials_exception


@router.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    try:
        # Connect to the database
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Retrieve the user's username and hashed password from the database
        query = "SELECT username, password, id, ROLE , id_compagny FROM users WHERE username = '" + form_data.username + "';"
        result = db.execute_query(query)

        print(result)
        if result:
            # Extract the username and hashed password from the result
            username, hashed_password, id, id_compagny, role = result[0]['username'], result[0]['password'], result[0]['id'], result[0]['id_compagny'], result[0]['ROLE']

            # Hash the entered password and compare it with the stored hashed password
            password_hash = hashlib.sha256(form_data.password.encode()).hexdigest()
            if password_hash == hashed_password:
                # If the passwords match, create a JWT token for authentication
                data = {"username": username, "id": id, "id_compagny": id_compagny, "role": role}
                jwt_token = jwt.encode(data, JWT_KEY, algorithm="HS256")

                # Return the generated access token and token type
                return {"access_token": jwt_token, "token_type": "bearer"}

        # If the username or password is incorrect, raise an HTTPException
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect username or password"
        )
    finally:
        # Close the database connection
        db.close_connection()
