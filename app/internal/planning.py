#System imports
from typing import Annotated

#Libs imports
from fastapi import Depends, APIRouter, HTTPException

#Local imports
from dbinterface.SQLDatabase import SQLDatabase
from internal.models import Planning , User
from internal.auth import decode_token

router = APIRouter()



@router.get("/plannings")
async def plannings_endpoint(user: Annotated[User, Depends(decode_token)]) -> list[Planning]:
        if user.role != "MAINTENER":
            raise HTTPException(status_code=401, detail="User is forbiden to access this company.")
                 
        
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

         # Execute a query
        query = "SELECT * FROM `planning`"
        result = db.execute_query(query)
        db.close_connection()

        return result

    
@router.post("/create_plannings/{id_company}")
async def create_planning(id_company: int, planning: Planning, user: Annotated[User, Depends(decode_token)]) -> int:
        if user.role != "MAINTENER":
            if user.company_id != id_company:
                raise HTTPException(status_code=403, detail="User is not authorized to access this company.")
            if user.role != "ADMIN":
                 raise HTTPException(status_code=401, detail="User is forbiden to access this company.")
                 
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Check if the company exists
        company_query = f"SELECT * FROM `compagnies` WHERE `id` = "+str(id_company)+ ";"
        company_result = db.execute_query(company_query)
        if not company_result:
            raise HTTPException(status_code=404, detail=f"Company with ID "+str(id_company)+" does not exist")

        # Create the planning in the database
        query = f"INSERT INTO `planning` (name, compagnie_id) VALUES ('"+str(planning.name)+"', '"+str(id_company)+"')"
        db.execute_query(query)
        db.connection.commit()
        planning_id = db.get_last_insert_id()

        db.close_connection()

        return planning_id
    
@router.delete("/delete_planning/{id}")
async def delete_planning(id: int, user: Annotated[User, Depends(decode_token)]):
        
        if user.role not in ["MAINTENER","ADMIN"]:
                raise HTTPException(status_code=403, detail="User is not authorized to access this company.")

                 
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Check if the planning exists
        planning_query = f"SELECT * FROM `planning` WHERE `id` = {id};"
        planning_result = db.execute_query(planning_query)
        if not planning_result:
            raise HTTPException(status_code=404, detail=f"Planning with ID {id} does not exist")

        # Delete the planning from the database
        query = f"DELETE FROM `planning` WHERE `id` = {id};"
        db.execute_query(query)
        db.connection.commit()

        db.close_connection()

        return {"message": f"Planning with ID {id} deleted successfully"}

    
@router.get("/planning/{id}")
async def get_planning(id: int, user: Annotated[User, Depends(decode_token)]) -> Planning:
        if user.role not in ["MAINTENER","ADMIN"]:
                raise HTTPException(status_code=403, detail="User is not authorized to access this company.")

        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Check if the planning exists
        query = f"SELECT * FROM `planning` WHERE `id` = {id};"
        result = db.execute_query(query)
        if not result:
            raise HTTPException(status_code=404, detail=f"Planning with ID {id} does not exist")

        db.close_connection()

        # Extract the planning data from the result
        planning_data = result[0]  # Assuming the query only returns one row

        # Create a Planning object from the retrieved data
        planning = Planning(**planning_data)

        return planning



@router.get("/company/{company_id}/plannings")
async def get_company_plannings(company_id: int, user: Annotated[User, Depends(decode_token)]) -> list[Planning]:
        if user.role != "MAINTENER":
            if user.company_id != company_id:
                raise HTTPException(status_code=403, detail="User is not authorized to access this company.")
            if user.role != "ADMIN":
                 raise HTTPException(status_code=401, detail="User is forbiden to access this company.")
                 
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Check if the company exists
        company_query = f"SELECT * FROM `compagnies` WHERE `id` = {company_id};"
        company_result = db.execute_query(company_query)
        if not company_result:
            raise HTTPException(status_code=404, detail=f"Company with ID {company_id} does not exist")

        # Retrieve all plannings of the company
        query = f"SELECT * FROM `planning` WHERE `compagnie_id` = {company_id};"
        result = db.execute_query(query)

        db.close_connection()

        # Create a list of Planning objects from the retrieved data
        plannings = [Planning(**planning_data) for planning_data in result]

        return plannings