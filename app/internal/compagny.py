#System imports
from typing import Annotated

#Libs imports
from fastapi import Depends, APIRouter, HTTPException
from jose import JWTError, jwt
#Local imports
from dbinterface.SQLDatabase import SQLDatabase
from internal.models import Company, User
from internal.auth import decode_token

router = APIRouter()



@router.get("/companies")
async def company_endpoint(user: Annotated[User, Depends(decode_token)] ) -> list[Company]:
        if user.role not in ["MAINTENER"]:
            raise HTTPException(status_code=401, detail="User is forbiden to access this company.")
        

        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

         # Execute a query
        query = "SELECT * FROM `compagnies`"
        result = db.execute_query(query)
        db.close_connection()

        return result
    

@router.get("/companie/{companyId}")
async def get_company_endpoint(companyId: int, user: Annotated[User, Depends(decode_token)])-> Company:
        if user.role not in ["ADMIN", "MAINTENEUR"]:
            raise HTTPException(status_code=401, detail="User is forbiden to access this company.")
        
        if user.company_id != companyId:
            raise HTTPException(status_code=403, detail="User is not authorized to access this company.")
        
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

         # Execute a query
        query = "SELECT * FROM `compagnies` WHERE `id` = "+str(companyId)+";"
        result = db.execute_query(query)
        db.close_connection()

        return result[0]
    

@router.post("/create_companie")
async def create_company_endpoint(Company: Company, user: Annotated[User, Depends(decode_token)]):
        if user.role not in ["MAINTENER"]:
            raise HTTPException(status_code=401, detail="User is forbiden to access this company.")
        
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

         # Execute a query
        query = "INSERT INTO `compagnies` (`id`, `siret`, `name`, `city`, `country`, `website`, `INIT`) VALUES (NULL, '"+str(Company.siret)+"', '"+str(Company.name)+"', '"+str(Company.city)+"', '"+str(Company.country)+"', '"+str(Company.website)+"', '2023-05-03');"
        result = db.execute_query(query)
        db.connection.commit()
        db.close_connection()

        return result
    

@router.delete("/delete_compagnie/{companyId}")
async def delete_company_endpoint(companyId: int, Company: Company, user: Annotated[User, Depends(decode_token)]):
        if user.role not in ["MAINTENER"]:
            raise HTTPException(status_code=401, detail="User is forbiden to access this company.")
        
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

         # Execute a query
        query = "DELETE FROM compagnies WHERE `compagnies`.`id` = "+str(companyId)+";"
        result = db.execute_query(query)
        db.connection.commit()
        db.close_connection()

        return result

    

@router.get("/company/{companyId}/users")
async def get_company_users_endpoint(companyId: int, user: Annotated[User, Depends(decode_token)]) -> list[User]:
        if user.role != "MAINTENER":
            if user.company_id != companyId:
                raise HTTPException(status_code=403, detail="User is not authorized to access this company.")
            if user.role != "ADMIN":
                 raise HTTPException(status_code=401, detail="User is forbiden to access this company.")
                 
        
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Execute a query
        query = f"SELECT * FROM users WHERE id_compagny = {companyId};"
        result = db.execute_query(query)
        db.close_connection()

        return result
    