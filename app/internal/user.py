#System imports
from typing import Dict, List  # Add Dict and List
from typing import Annotated

#Libs imports
from fastapi import Depends, APIRouter, HTTPException

#Local imports
from dbinterface.SQLDatabase import SQLDatabase
from internal.models import User, Activities
from internal.auth import decode_token

router = APIRouter()

@router.get("/users")
async def get_all_users(user: Annotated[User, Depends(decode_token)]) -> list[User]:
    try:
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Execute a query to retrieve all users
        query = "SELECT id, username, firstname, lastname, ROLE FROM `users`"
        result = db.execute_query(query)

        db.close_connection()

        # Create a list of User objects from the retrieved data
        users = [User(**user_data) for user_data in result]

        return users
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/users/{user_id}/activities-by-company")
async def get_user_activities_by_company(user_id: int, user: Annotated[User, Depends(decode_token)]) -> Dict[str, List[Activities]]:
    try:
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Check if the user exists
        user_query = f"SELECT * FROM `users` WHERE `id` = {user_id}"
        user_result = db.execute_query(user_query)
        if not user_result:
            raise HTTPException(status_code=404, detail=f"User with ID {user_id} does not exist")

        # Execute a query to retrieve activities of the user by company
        query = f"""
            SELECT c.name AS company_name, p.name AS planning_name, a.*
            FROM `activities` a
            JOIN `planning` p ON a.planning_id = p.id
            JOIN `compagnies` c ON p.compagnie_id = c.id
            WHERE a.owner_id = {user_id}
            ORDER BY c.name, p.name
        """
        result = db.execute_query(query)

        db.close_connection()

        # Create a dictionary of company names mapped to a list of activities
        activities_by_company = {}
        for activity_data in result:
            company_name = activity_data['company_name']
            activity = Activities(**activity_data)
            if company_name not in activities_by_company:
                activities_by_company[company_name] = []
            activities_by_company[company_name].append(activity)

        return activities_by_company
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
