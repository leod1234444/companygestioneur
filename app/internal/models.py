#System imports

#Libs imports
from pydantic import BaseModel

#Local imports

class User(BaseModel): # we don't include password_hash in the definition of the class because we don't want to return it
    id: int
    username: str
    firstname: str
    lastname: str
    ROLE: str


class Company(BaseModel):
    id: int = None
    siret: int = None
    name: str
    city: str
    country: str
    website: str = None
    #users: list[User] = []


class Planning(BaseModel):
    id: int = None
    name: str


class Activities(BaseModel):
    id: int = None
    name: str
    goal: str
    description: str
    localisation: str