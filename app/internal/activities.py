#System imports
from typing import Annotated

#Libs imports
from fastapi import Depends, APIRouter, HTTPException

#Local imports
from dbinterface.SQLDatabase import SQLDatabase
from internal.models import Activities, User
from internal.auth import decode_token

router = APIRouter()

@router.get("/activities")
async def get_all_activities(user: Annotated[User, Depends(decode_token)]) -> list[Activities]:
    try:
        if user.role not in ["MAINTENER"]:
            raise HTTPException(status_code=401, detail="User is forbiden to access this company.")
        
        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Execute a query to retrieve all activities
        query = "SELECT * FROM `activities`"
        result = db.execute_query(query)

        db.close_connection()

        # Create a list of Activity objects from the retrieved data
        activities = [Activities(**activity_data) for activity_data in result]

        return activities
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.post("/activities/{planning_id}/{owner_id}")
async def create_activity(planning_id: int,owner_id: int, activity: Activities, user: Annotated[User, Depends(decode_token)]) -> int:
        if user.role not in ["MAINTENER","ADMIN"]:
                raise HTTPException(status_code=403, detail="User is not authorized to access this company.")

        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Check if the planning exists
        planning_query = f"SELECT * FROM `planning` WHERE `id` = {planning_id};"
        planning_result = db.execute_query(planning_query)
        if not planning_result:
            raise HTTPException(status_code=404, detail=f"Planning with ID {planning_id} does not exist")

        # Create the activity in the database and link it to the planning
        query = f"INSERT INTO `activities` (name, owner_id, planning_id, description, goal, localisation) \
                 VALUES ('{activity.name}', {owner_id}, {planning_id}, '{activity.description}', \
                         '{activity.goal}', '{activity.localisation}')"
        db.execute_query(query)
        db.connection.commit()
        activity_id = db.get_last_insert_id()

        db.close_connection()

        return activity_id
    

@router.delete("/activities/{activity_id}")
async def delete_activity(activity_id: int,user: Annotated[User, Depends(decode_token)]):
        if user.role not in ["MAINTENER","ADMIN"]:
                raise HTTPException(status_code=403, detail="User is not authorized to access this company.")

        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Check if the activity exists
        activity_query = f"SELECT * FROM `activities` WHERE `id` = {activity_id};"
        activity_result = db.execute_query(activity_query)
        if not activity_result:
            raise HTTPException(status_code=404, detail=f"Activity with ID {activity_id} does not exist")

        # Delete the activity from the database
        query = f"DELETE FROM `activities` WHERE `id` = {activity_id};"
        db.execute_query(query)
        db.connection.commit()
        db.close_connection()

        return {"message": f"Activity with ID {activity_id} deleted successfully"}


@router.get("/activities/{activity_id}")
async def get_activity(activity_id: int, user: Annotated[User, Depends(decode_token)]) -> Activities:
        if user.role not in ["MAINTENER","ADMIN"]:
                raise HTTPException(status_code=403, detail="User is not authorized to access this company.")

        db = SQLDatabase(host='db', port=3306, database='app_db', username='db_user', password='db_user_pass')
        db.connect()

        # Check if the activity exists
        activity_query = f"SELECT * FROM `activities` WHERE `id` = {activity_id};"
        activity_result = db.execute_query(activity_query)
        if not activity_result:
            raise HTTPException(status_code=404, detail=f"Activity with ID {activity_id} does not exist")

        db.close_connection()

        # Extract the activity data from the result
        activity_data = activity_result[0]  # Assuming the query only returns one row

        # Create an Activity object from the retrieved data
        activity = Activities(**activity_data)

        return activity


