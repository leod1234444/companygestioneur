import pymysql

class SQLDatabase:
    def __init__(self, host, port, database, username, password):
        self.host = host
        self.port = port
        self.database = database
        self.username = username
        self.password = password
        self.connection = None
        self.cursor = None

    def connect(self):
        try:
            self.connection = pymysql.connect(
                host=self.host,
                port=self.port,
                user=self.username,
                password=self.password,
                database=self.database,
                cursorclass=pymysql.cursors.DictCursor
            )
            self.cursor = self.connection.cursor()
            print("Connected to the database.")
        except pymysql.Error as e:
            print(f"Error connecting to the database: {str(e)}")

    def execute_query(self, query):
        try:
            self.cursor.execute(query)
            rows = self.cursor.fetchall()
            return rows
        except pymysql.Error as e:
            print(f"Error executing query: {str(e)}")

    def close_connection(self):
        self.cursor.close()
        self.connection.close()
        print("Connection closed.")

    def get_last_insert_id(self):
        try:
            self.cursor.execute("SELECT LAST_INSERT_ID()")
            last_insert_id = self.cursor.fetchone()["LAST_INSERT_ID()"]
            return last_insert_id
        except pymysql.Error as e:
            print(f"Error getting last insert ID: {str(e)}")